#include <stdio.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <stdlib.h>
#include <pthread.h>
#include <semaphore.h>

#define MAX_RAND 100

sem_t ints;
sem_t space;
sem_t mutex;

typedef struct shared_memory {
	int N;
	int *buf;
	int in, out;
	int id;
} shared;

void *send (void *arg){
	//while(1){
		shared *args = (shared *)arg;
		
		sem_wait (&space);
		sem_wait (&mutex);
		
		args->in = (args->in+1)%args->N;
		args->buf[args->in] = rand() % MAX_RAND;
		printf("\nHilo %d produjo en ítem. Hay %d elementos en la cola\n", args->id, args->in);

		sem_post(&mutex);
		sem_post(&ints);

		
		//usleep(1);
	//}
}

void *rcv (void *arg){
	//while(1){
		int c;
		shared *args = (shared *)arg;
		
		sem_wait(&ints);
		sem_wait(&mutex);

		c = args->buf[args->out];
		args->out = (args->out+1)%args->N;
		args->buf[args->out] = 0;
		printf("\nHilo %d consumió un ítem. Hay %d elementos en la cola\n", args->id, args->out);

		sem_post(&mutex);
		sem_post(&space);

		return (void *) c;
		//usleep(3);
	//}
}	

int main(int argc, char **argv){
	/*
	char option;
	shared args;
	while (( option = getopt(argc, argv, "p:c:n:e:s:")) !=-1){
  		switch(option){
        //NUMERO HILOS PRODUCTORES
        case 'p':
			n_producers = atoi(optarg);
			break;
        //NUMERO HILOS CONSUMIDOS
		case 'c':
			n_consumers = atoi(optarg);
			break;
        //TAMAÑO DEL BUFFERnal
		case 'n':
			args.N = atoi(optarg);
			break;
		
		//NUMERO DE ELEMENTOS A PRODUCIR
		case 'e':
			numElementosP = atoi(optarg);
			break;
		
		//SI EXISTE HARÀ QUE EL PROGRAMA USE SEMAFORO
		case 's':
			sem = 1;
			break;
		
		default:
			printf("Argumento no valido\n");
			break;
  		}
  	}
	*/
	
	shared args;
	args.N = 5;
	args.buf = malloc(args.N*sizeof(int));

	
	sem_init(&mutex, 0, 1);
	sem_init(&ints, 0, 0);
	sem_init(&space, 0, args.N);
	
	int n_producers = 5;
	int n_consumers = 5;
	
	while (1) {
		if (n_producers >= 0) {
			pthread_t tid = (pthread_t) n_producers;
			//args.id = i;
			pthread_create(&tid, NULL, send, (void *) &args);
			n_producers--;
		}
		
		else if (n_consumers >= 0) {
			pthread_t tid = (pthread_t) n_consumers;
			//args.id = i;
			pthread_create(&tid, NULL, rcv, (void *) &args);
			n_consumers--;
		}
	}
	
	
	


}
